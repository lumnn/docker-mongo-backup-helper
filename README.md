# Mongo Backup Helper

> A Docker image to help managing docker backup

## Requirements

- File access to Mongo data files
- Network access to Mongo server
- Admin permissions to Mongo server

## Usage

There are three scripts that can be used with this image

### `backup_mongo_files.sh` - Copies mongo files to desired location.

Can be controlled with following ENV variables:

- `BACKUP_LOCATION` - for specifying where copy the backup files to
- `MONGO_DATA` - optionally, if mongo data is not stored at `/data/db`

Script will copy `/data/db` files into new folder in `$BACKUP_LOCATION`. Additionally a link named `latest` will be created in `$BACKUP_LOCATION`, which will point to latest copied backup.

### `restore_mongo_files.sh` - Seeds mongo `/data/db` with previously copied files

Copies files from the backup place into Mongo `/data/db` location.

Command will take an argument specifying which backup folder use to seed from.
Optionally it can be specified with `$RESTORE_BACKUP_NAME` variable.

- `BACKUP_LOCATION` - Same as above
- `MONGO_DATA` - Same as above
- `RESTORE_BACKUP_NAME` - A name of the backup to restore

### `backup_mongo.sh` - Locks mongo and copies files

Allows copying files form Mongo. Will lock Mongo for the time when files are copied.

Available options:

- `-s, --schedule` Schedule mode, value expected is a cron like schedule.  
For example: 0 3 * * *  
Will only make backups on primary instance.
- `-1, --primary` (Not availalbe in schedule mode) Allow backups when state of the instance is primary
- `-2, --secondary` (Not availalbe in schedule mode) Allow backups when state of the instance is secondary

Optional ENV variables:

- `MONGO_USER` - Mongo username
- `MONGO_PASSWORD` - Mongo password
- `MONGO_HOST` - Mongo host
