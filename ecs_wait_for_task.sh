#!/usr/bin/env sh

set -eu

usage() {
  cat <<EOF
Usage: $0 [--desired-status-checks=N] [--known-status-checks=N] TASK_NAME EXPECTED_STATUS

Available options:

-w, --wait-for-task-checks   (Default: 30) Number of checks with 1s wait between for task to appear in metadata
-d, --desired-status-checks  (Default: 30) Number of checks with 1s wait between for desired status
-k, --known-status-checks    (Default: 30) Number of checks with 1s wait between for known status
-h, --help                   Print this help and exit
-v, --verbose                Print script debug info

EOF
  exit
}

msg() {
  echo >&2 "ECS Wait:" "${1-}"
}

die() {
  msg "$1"
  exit "${2-1}"
}

parse_params() {
  wait_for_task_checks=30
  desired_status_checks=30
  known_status_checks=30

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -w | --wait-for-task-checks)
        [ "$2" -gt 1 ]
        wait_for_task_checks="${2}"
        shift
        ;;
    -d | --desired-status-checks)
        [ "$2" -gt 1 ]
        desired_status_checks="${2}"
        shift
        ;;
    -k | --known-status-checks)
        [ "$2" -gt 1 ]
        known_status_checks="${2}"
        shift
        ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  task_name=$1
  wait_status=$2

  return 0
}

parse_params "$@"

get_mongo_container_info () {
    curl -s "${ECS_CONTAINER_METADATA_URI_V4}/task" | jq -r ".Containers[] | select(.Name == \"$task_name\")"
}

msg "Checking '$task_name' for '$wait_status'"

mongo_container=$(get_mongo_container_info)

while true; do
    if [ -n "$mongo_container" ]; then
        break
    fi

    if [ "$wait_for_task_checks" -lt 1 ]; then
        die "Missing information on mongo container. Exiting" 150
    fi

    echo "Missing information on mongo container. Will check ${wait_for_task_checks} more time(s)"

    sleep 1
    wait_for_task_checks=$((wait_for_task_checks-1))
    mongo_container=$(get_mongo_container_info)
done

while true; do
    if [ -z "$mongo_container" ]; then
        die "Missing information on mongo container, while checking desired status" 150
    fi

    desired_status=$(jq -r -n --argjson data "$mongo_container" '$data.DesiredStatus')
    msg "Expecting task desired status to be ${wait_status}. Got: ${desired_status}. Iteration ${desired_status_checks}"

    if [ "$wait_status" = "$desired_status" ]; then
        break
    fi

    if [ "$desired_status_checks" -lt 1 ]; then
        die "Giving up on checking desired status" 151
    fi

    sleep 1
    desired_status_checks=$((desired_status_checks-1))
    mongo_container=$(get_mongo_container_info)
done

while true; do
    if [ -z "$mongo_container" ]; then
        die "Missing information on mongo container, while checking known status" 150
    fi

    known_status=$(jq -r -n --argjson data "$mongo_container" '$data.KnownStatus')
    msg "Expecting task known status to be ${wait_status}. Got: ${known_status}. Iteration ${known_status_checks}"

    if [ "$wait_status" = "$known_status" ]; then
        break
    fi

    if [ "$known_status_checks" -lt 1 ]; then
        die "Giving up on checking known status" 151
    fi

    sleep 1
    known_status_checks=$((known_status_checks-1))
    mongo_container=$(get_mongo_container_info)
done

# If waiting for task stop, then we output its exit code
if [ "STOPPED" = "$wait_status" ]; then
    exit_code=$(jq -r -n --argjson data "$mongo_container" '$data.ExitCode')

    msg "Mongo container is now $known_status and exit code is $exit_code"
    echo "$exit_code"
    exit 0
fi

# Otherwise just status
echo "$known_status"
