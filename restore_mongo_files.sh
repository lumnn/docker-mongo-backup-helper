#!/usr/bin/env sh

set -e

BACKUP_LOCATION=${BACKUP_LOCATION:-/backups}
MONGO_DATA=${MONGO_DATA:-/data/db}

RESTORE_BACKUP_NAME=${1:-$RESTORE_BACKUP_NAME}
echo "Trying to restore $RESTORE_BACKUP_NAME"

if [ ! -d "$BACKUP_LOCATION/$RESTORE_BACKUP_NAME" ]; then
    file_exists=$(find "${BACKUP_LOCATION}" -type f)

    if [ -z "$file_exists" ]; then
        echo "Backup location seems to be empty. Therefore we allow restoration to fail."
        exit 0
    fi

    echo "Location '$BACKUP_LOCATION/$RESTORE_BACKUP_NAME' doesn't exist"

    ls -la "$BACKUP_LOCATION"

    exit 1
fi

cp -p -R -T "$BACKUP_LOCATION/$RESTORE_BACKUP_NAME/" "$MONGO_DATA"
