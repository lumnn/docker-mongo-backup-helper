FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
  curl \
  jq \
  gnupg2 \
  cron \
  && rm -rf /var/lib/apt/lists/*

RUN curl -fsSL https://www.mongodb.org/static/pgp/server-3.6.asc | apt-key add -
RUN echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/3.6 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.6.list

RUN apt-get update && apt-get install -y mongodb-org-shell \
  && rm -rf /var/lib/apt/lists/*

COPY backup_mongo.sh backup_mongo_files.sh ecs_wait_for_task.sh restore_mongo_files.sh /usr/local/bin/
