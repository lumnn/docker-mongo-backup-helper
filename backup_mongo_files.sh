#!/usr/bin/env sh

set -e

BACKUP_LOCATION=${BACKUP_LOCATION:-/backups}
MONGO_DATA=${MONGO_DATA:-/data/db}

backup_name=$(date +%s)_$(hostname)

cp -p -R "$MONGO_DATA" "$BACKUP_LOCATION/${backup_name}.incomplete"
mv "$BACKUP_LOCATION/${backup_name}.incomplete" "$BACKUP_LOCATION/${backup_name}"
ln -sfn "./${backup_name}" "$BACKUP_LOCATION/latest"

echo >&2 "Backing up files completed."
echo "$backup_name"
