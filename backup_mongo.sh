#!/usr/bin/env sh

set -eu

usage() {
  cat <<EOF
Available options:

-h, --help          Print this help and exit
-v, --verbose       Print script debug info
-s, --schedule      Schedule mode, value expected is a cron like schedule.
                    For example: 0 3 * * *
                    Will only make backups on primary instance.
-1, --primary       (Not availalbe in schedule mode) Allow backups when state of the instance is primary
-2, --secondary     (Not availalbe in schedule mode) Allow backups when state of the instance is secondary

Optional ENV variables:

MONGO_USER           Mongo username
MONGO_PASSWORD       Mongo password
MONGO_HOST           Mongo host
EOF
  exit
}

die() {
  echo "$1"
  exit "${2-1}"
}

parse_params() {
  # default values of variables set from params
  primary=0
  secondary=0
  schedule=

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -s | --schedule)
        schedule="${2-}"
        shift
        ;;
    -1 | --primary) primary=1 ;; # example primary
    -2 | --secondary) secondary=1 ;; # example primary
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  return 0
}

parse_params "$@"

eval_mongo () {
    params=""

    if [ -n "${MONGO_USER:-}" ]; then
        params="$params --username $MONGO_USER"
    fi

    if [ -n "${MONGO_PASSWORD:-}" ]; then
        params="$params --password $MONGO_PASSWORD"
    fi

    if [ -n "${MONGO_HOST:-}" ]; then
        params="$params --host $MONGO_HOST"
    fi

    echo "Mongo Exec: $1" >&2

    # shellcheck disable=SC2086
    mongo $params --quiet --eval "$1"
}

stop_cron () {
    echo "Stopping... Cron pid: $cron_pid"
    kill -TERM "$cron_pid"
}

is_state_compatible () {
    state=$1

    if [ 1 = "$primary" ] && [ 1 = "$state" ]; then
        echo 1
    fi

    if [ 1 = "$secondary" ] && [ 2 = "$state" ]; then
        echo 1
    fi

    echo 0
}

on_demand () {
    echo "Making on demand backup"

    backup_mongo.sh --primary --secondary || true

    wait_with_cron
}

wait_with_cron () {
    set +e
    wait "$cron_pid"
    set -e
    cron_exit_code=$?
    echo "Cron stopped with exit code ${cron_exit_code}" $cron_exit_code

    trap - TERM USR1

    mongo_exit_code=""
    if [ -n "$ECS_CONTAINER_METADATA_URI_V4" ]; then
        mongo_exit_code=$(ecs_wait_for_task.sh "mongo" "STOPPED")
    fi;

    if [ 0 = "$mongo_exit_code" ]; then
        echo "Doing final backup"

        backup_mongo_files.sh
    fi;

    exit
}

if [ -n "$schedule" ]; then
    if [ -n "$ECS_CONTAINER_METADATA_URI_V4" ]; then
        echo "Waiting for Mongo to start"
        ecs_wait_for_task.sh --known-status-checks 60 "mongo" "RUNNING"
    fi

    echo "Entering schedule mode"
    echo "Installing cron with schedule $schedule"

    echo "$schedule kill -USR1 $$" | crontab -

    cron -f &
    cron_pid=$!

    trap "on_demand" USR1
    trap "stop_cron" TERM

    wait_with_cron
fi

state=$(eval_mongo "rs.status().myState")
if [ "1" != "$(is_state_compatible "$state")" ]; then
    echo "Mongo is in an unexpected state - $state"
fi

# for logs really
eval_mongo "rs.printSecondaryReplicationInfo()"

echo "Locking Mongo"
eval_mongo "db.fsyncLock()"

echo "Copying files"
backup_mongo_files.sh || true

echo "Unlocking Mongo"
eval_mongo "db.fsyncUnlock()"

echo "Backup completed"
